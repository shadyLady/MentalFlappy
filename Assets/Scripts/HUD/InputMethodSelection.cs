﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System; 

public class InputMethodSelection : MonoBehaviour
{
	[SerializeField]
	private PlayerSettings _playerSettings;

	[SerializeField] 
	private TextMeshProUGUI _inputMethodLabel; 

	private int _maxValue = 1;

	private int _currentInputMethod = 0;

	public Action<InputMethod> InputMethodUpdated;

	private void UpdateInputMethod()
	{
		InputMethod method = (InputMethod)_currentInputMethod;

		_playerSettings.inputMethod = method;

		if (InputMethodUpdated != null)
			InputMethodUpdated(method);

		_inputMethodLabel.text = method.ToString();
	}

	public void IncreaseInputMethod()
	{
		_currentInputMethod++;

		if (_currentInputMethod > _maxValue)
			_currentInputMethod = 0;

		UpdateInputMethod(); 
	}

	public void DecreaseInputMethod()
	{
		_currentInputMethod--;

		if (_currentInputMethod < 0)
			_currentInputMethod = _maxValue;

		UpdateInputMethod(); 
	}
}
