﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitForNeuroskyConnection : MonoBehaviour
{
	[SerializeField]
	private TGCConnectionController _connectionController;

	[SerializeField]
	private InputMethodSelection _inputMethodSelection;

	private Button _button;

	private void Awake()
	{
		_button = GetComponent<Button>();

		_inputMethodSelection.InputMethodUpdated += OnInputMethodUpdated;
	}

	private void OnInputMethodUpdated(InputMethod method)
	{
		if (method != InputMethod.Neurosky)
		{
			_button.interactable = true;
			return;
		}

		_connectionController.Parser.UpdatePoorSignalEvent += UpdatePoorSignalEvent;

		_button.interactable = false;
	}

	private void UpdatePoorSignalEvent(int value)
	{
		if (value == 0)
		{
			_connectionController.Parser.UpdatePoorSignalEvent -= UpdatePoorSignalEvent;
			_button.interactable = true;
		}
	}
}
