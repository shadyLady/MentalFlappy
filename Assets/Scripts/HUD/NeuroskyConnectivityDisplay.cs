﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuroskyConnectivityDisplay : MonoBehaviour
{
	[SerializeField]
	private TGCConnectionController _connectionController; 

	[SerializeField]
	private InputMethodSelection _inputMethodSelection;

	[SerializeField]
	private GameObject _displayPanel; 

	private void Awake()
	{
		_inputMethodSelection.InputMethodUpdated += OnInputMethodUpdated; 
	}

	private void OnInputMethodUpdated(InputMethod method)
	{
		switch (method)
		{
			case InputMethod.Keyboard: Hide(); break;
			case InputMethod.Neurosky: Show(); break;
		}
	}

	private void Show()
	{
		if (_displayPanel.activeSelf)
			return;

		_connectionController.Connect(); 

		_displayPanel.SetActive(true); 
	}

	private void Hide()
	{
		if (!_displayPanel.activeSelf)
			return;

		_connectionController.Disconnect();

		_displayPanel.SetActive(false);
	}
}
