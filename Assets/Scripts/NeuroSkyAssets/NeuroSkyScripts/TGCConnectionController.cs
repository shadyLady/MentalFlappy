using UnityEngine;
using System.Net.Sockets;
using System.Text;
using System.IO;
using System.Threading; 

public class TGCConnectionController : MonoBehaviour
{
	public ConnectionDataParser Parser;

	private Thread _connectionThread;

	private TcpClient _client; 

  	private Stream _stream;

  	private byte[] _buffer;	

	void Awake ()
	{
        Parser = new ConnectionDataParser();

		DontDestroyOnLoad(this);
	}
	
	public void Disconnect()
	{
		if(_connectionThread != null && _connectionThread.IsAlive)
		{
            _connectionThread.Abort();
			_stream.Close();
		}
	}
	
	public void Connect()
	{
		if(_connectionThread == null || !_connectionThread.IsAlive){

            _client = new TcpClient("127.0.0.1", 13854);
            _stream = _client.GetStream();
            _buffer = new byte[1024];

            Parser.Assign(_stream, _client, _buffer);

            byte[] myWriteBuffer = Encoding.ASCII.GetBytes(@"{""enableRawOutput"": true, ""format"": ""Json""}");
		    _stream.Write(myWriteBuffer, 0, myWriteBuffer.Length);

            _connectionThread = new Thread(new ThreadStart(Parser.ParseData));
            _connectionThread.Start();
		}
	}
	
	void OnApplicationQuit()
	{
		Disconnect();
	}
}


