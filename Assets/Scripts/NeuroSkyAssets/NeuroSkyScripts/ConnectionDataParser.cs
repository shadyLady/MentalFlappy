﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Text;
using Jayrock.Json.Conversion;
using System.Net.Sockets; 

public class ConnectionDataParser 
{
    private byte[] buffer;
    private Stream stream;
    private TcpClient client; 

    public delegate void UpdateIntValueDelegate(int value);
    public delegate void UpdateFloatValueDelegate(float value);

    public event UpdateIntValueDelegate UpdatePoorSignalEvent;
    public event UpdateIntValueDelegate UpdateAttentionEvent;
    public event UpdateIntValueDelegate UpdateMeditationEvent;
    public event UpdateIntValueDelegate UpdateRawdataEvent;
    public event UpdateIntValueDelegate UpdateBlinkEvent;

    public event UpdateFloatValueDelegate UpdateDeltaEvent;
    public event UpdateFloatValueDelegate UpdateThetaEvent;
    public event UpdateFloatValueDelegate UpdateLowAlphaEvent;
    public event UpdateFloatValueDelegate UpdateHighAlphaEvent;
    public event UpdateFloatValueDelegate UpdateLowBetaEvent;
    public event UpdateFloatValueDelegate UpdateHighBetaEvent;
    public event UpdateFloatValueDelegate UpdateLowGammaEvent;
    public event UpdateFloatValueDelegate UpdateHighGammaEvent;

    public void Assign(Stream s, TcpClient c, byte[] b)
    {
        buffer = b;
        stream = s;
        client = c;
    }

    public void ParseData()
    {
        while(true)
        {
            if (stream.CanRead)
            {
                try
                {
                    int bytesRead = stream.Read(buffer, 0, buffer.Length);

                    string[] packets = Encoding.ASCII.GetString(buffer, 0, bytesRead).Split('\r');

                    foreach (string packet in packets)
                    {
                        if (packet.Length == 0)
                            continue;

                        IDictionary primary = (IDictionary)JsonConvert.Import(typeof(IDictionary), packet);

                        if (primary.Contains("poorSignalLevel"))
                        {

                            if (UpdatePoorSignalEvent != null)
                            {
                                UpdatePoorSignalEvent(int.Parse(primary["poorSignalLevel"].ToString()));
                            }

                            if (primary.Contains("eSense"))
                            {
                                IDictionary eSense = (IDictionary)primary["eSense"];
                                if (UpdateAttentionEvent != null)
                                {
                                    UpdateAttentionEvent(int.Parse(eSense["attention"].ToString()));
                                }
                                if (UpdateMeditationEvent != null)
                                {
                                    UpdateMeditationEvent(int.Parse(eSense["meditation"].ToString()));
                                }
                            }

                            if (primary.Contains("eegPower"))
                            {
                                IDictionary eegPowers = (IDictionary)primary["eegPower"];

                                if (UpdateDeltaEvent != null)
                                {
                                    UpdateDeltaEvent(float.Parse(eegPowers["delta"].ToString()));
                                }
                                if (UpdateThetaEvent != null)
                                {
                                    UpdateThetaEvent(float.Parse(eegPowers["theta"].ToString()));
                                }
                                if (UpdateLowAlphaEvent != null)
                                {
                                    UpdateLowAlphaEvent(float.Parse(eegPowers["lowAlpha"].ToString()));
                                }
                                if (UpdateHighAlphaEvent != null)
                                {
                                    UpdateHighAlphaEvent(float.Parse(eegPowers["highAlpha"].ToString()));
                                }
                                if (UpdateLowBetaEvent != null)
                                {
                                    UpdateLowBetaEvent(float.Parse(eegPowers["lowBeta"].ToString()));
                                }
                                if (UpdateHighBetaEvent != null)
                                {
                                    UpdateHighBetaEvent(float.Parse(eegPowers["highBeta"].ToString()));
                                }
                                if (UpdateLowGammaEvent != null)
                                {
                                    UpdateLowGammaEvent(float.Parse(eegPowers["lowGamma"].ToString()));
                                }
                                if (UpdateHighGammaEvent != null)
                                {
                                    UpdateHighGammaEvent(float.Parse(eegPowers["highGamma"].ToString()));
                                }
                            }
                        }
                        else if (primary.Contains("rawEeg") && UpdateRawdataEvent != null)
                        {
                            UpdateRawdataEvent(int.Parse(primary["rawEeg"].ToString()));
                        }
                        else if (primary.Contains("blinkStrength") && UpdateBlinkEvent != null)
                        {
                            UpdateBlinkEvent(int.Parse(primary["blinkStrength"].ToString()));
                        }
                    }
                }
                catch (IOException e) { Debug.Log("IOException " + e); }
                catch (System.Exception e) { Debug.Log("Exception " + e); }
            }
        }
    
    }
}
