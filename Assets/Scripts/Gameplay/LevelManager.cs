﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	[SerializeField]
	private GameObject[] _levelSegments;

	private List<GameObject> _activeSegments;

	[SerializeField]
	private float _levelSpeed; 

	[SerializeField]
	private float _spawnDelay; 

	private float _timeToNextSpawn; 

	private float _timePassed;

	private Vector2 _horizontalLevelBounds; 

	private void Awake()
	{
		_activeSegments = new List<GameObject>();

		Camera cam = Camera.main;

		_horizontalLevelBounds = new Vector2(
			cam.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, cam.transform.position.z)).x,
			cam.ViewportToWorldPoint(new Vector3(1.0f, 1.0f, cam.transform.position.z)).x); 
	}

	private void Update()
	{
		_timePassed += Time.deltaTime;

		if (_timePassed > _timeToNextSpawn)
		{
			_timeToNextSpawn += _spawnDelay;

			foreach (GameObject obj in _levelSegments)
			{
				if (!obj.activeSelf)
				{
					_activeSegments.Add(obj);

					obj.transform.position = new Vector3(
						_horizontalLevelBounds.y, 
						obj.transform.position.y);

					obj.SetActive(true);

					break;
				}
			}
		}

		for (int idx = _activeSegments.Count - 1; idx >= 0; idx--)
		{
			Vector3 newLocation = _activeSegments[idx].transform.position + ((transform.right * _levelSpeed) * Time.deltaTime);
			_activeSegments[idx].transform.position = newLocation;

			if (newLocation.x < _horizontalLevelBounds.x)
			{
				_activeSegments[idx].SetActive(false);
				_activeSegments.RemoveAt(idx); 
			}
		}
	}
}
