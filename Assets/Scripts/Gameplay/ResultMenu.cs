﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class ResultMenu : MonoBehaviour
{
	[SerializeField]
	private GameManager _gameManager;

	[SerializeField]
	private ScoreManager _scoreManager; 

	[SerializeField]
	private GameObject _menuPanel;

	[SerializeField]
	private TextMeshProUGUI _scoreText; 

	private void Awake()
	{
		_gameManager.GameOverEvent += OnGameOver; 
	}

	private void OnGameOver()
	{
		_scoreText.text = _scoreManager.TotalScore.ToString(); 
		_menuPanel.SetActive(true); 
	}

	public void OnRestartClicked()
	{
		_gameManager.RestartGame(); 
	}

	public void OnQuitClicked()
	{
		
	}
}
