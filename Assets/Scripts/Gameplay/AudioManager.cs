﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	private AudioSource _backgroundSource;

	private List<AudioSource> _sfxSources;

	private int _sfxSourceCount = 5;

	private bool _audioMuted; 

	private void Awake()
	{
		_backgroundSource = gameObject.AddComponent<AudioSource>();

		_sfxSources = new List<AudioSource>();

		for (int idx = 0; idx < _sfxSourceCount; idx++)
		{
			_sfxSources.Add(gameObject.AddComponent<AudioSource>());
		}

		if (PlayerPrefs.HasKey("volume"))
			AudioListener.volume = PlayerPrefs.GetFloat("volume"); 
	}

	public void PlaySound(AudioClip clip)
	{
		foreach (AudioSource source in _sfxSources)
		{
			if (!source.isPlaying)
			{
				source.PlayOneShot(clip);
				return; 
			}
		}
	}

	public void MuteAudio()
	{
		_audioMuted = !_audioMuted;

		if (_audioMuted)
			PlayerPrefs.SetFloat("volume", 0.0f);
		else PlayerPrefs.SetFloat("volume", 1.0f); 
	}
}
