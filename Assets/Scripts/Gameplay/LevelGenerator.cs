﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

	[SerializeField]
	private WallGenerator _topWall;

	[SerializeField]
	private WallGenerator _bottomWall;

	[SerializeField]
	private Transform _targetTransform; 

	[SerializeField]
	private float _spawnDistance = 0.5f;

	private float _timePassed;

	private Camera _camera;

	private float _wallOffset = 0.1f;

	private float _levelHeight; 

	private void Awake()
	{
		_camera = Camera.main;

		_levelHeight = 1.0f - (2 * _wallOffset); 
	}

	private void Update()
	{
		_timePassed += Time.deltaTime;

		if (_timePassed < _spawnDistance)
			return;

		_timePassed = 0.0f;

		float nextAmplitude = _wallOffset + ((_camera.WorldToViewportPoint(_targetTransform.position).y / _levelHeight));

		_topWall.GenerateCube(nextAmplitude);
		_bottomWall.GenerateCube(nextAmplitude);
	}
}
