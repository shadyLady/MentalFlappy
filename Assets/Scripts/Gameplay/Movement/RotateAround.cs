﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MonoBehaviour
{
	[SerializeField]
	private float _rotationSpeed;

	private float _angle; 

	private void Update()
	{
		_angle += Mathf.PI * (_rotationSpeed * Time.deltaTime);

		transform.rotation = Quaternion.AngleAxis(_angle, transform.forward);  
	}
}
