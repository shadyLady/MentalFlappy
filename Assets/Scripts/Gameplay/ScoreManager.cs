﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
	[SerializeField]
	private PlayerController _player;

	[SerializeField]
	private TextMeshProUGUI _scoreText; 

	private int _totalScore;

	public int TotalScore { get { return _totalScore; } }

	private void Awake()
	{
		_player.ScoreEvent += OnScoreEvent; 
	}

	private void OnScoreEvent(int value)
	{
		_totalScore += value;

		_scoreText.text = _totalScore.ToString(); 
	}

}
