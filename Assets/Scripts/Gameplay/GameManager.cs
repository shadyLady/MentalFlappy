﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 
using System; 

public class GameManager : MonoBehaviour
{
	[SerializeField]
	private PlayerController _player;

	public Action GameOverEvent;

	public Action GamePausedEvent;

	public Action GameResumedEvent; 

	private void Awake()
	{
		_player.GameOverEvent += OnGameOver; 
	}

	private void OnGameOver()
	{
		PauseGame(true);

		if (GameOverEvent != null)
			GameOverEvent(); 
	}

	private void PauseGame(bool pause)
	{
		if (pause) Time.timeScale = 0.0f;
		else Time.timeScale = 1.0f; 
	}

	public void RestartGame()
	{
		PauseGame(false); 

		SceneManager.LoadScene("Main"); 
	}

	public void QuitGame()
	{
		PauseGame(false);

		SceneManager.LoadScene("MainMenu"); 
	}
}
