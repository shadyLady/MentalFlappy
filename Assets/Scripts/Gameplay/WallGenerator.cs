﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
	[SerializeField]
	private GameObject _cubePrefab;

	[SerializeField]
	private float _minAmplitude = 0.2f;

	[SerializeField]
	private float _maxAmplitude = 2.6f;

	[SerializeField]
	private float _frequency = 2.0f;

	[SerializeField]
	private bool _invertWave; 

	private List<GameObject> _cubes;

	private Vector2 _levelBounds;

	private float _amplitude;

	private float _spawnDistance = 0.45f;

	private Camera _camera; 

	private void Awake()
	{
		_camera = Camera.main;

		_levelBounds = new Vector2(
			_camera.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, _camera.transform.position.z)).x,
			_camera.ViewportToWorldPoint(new Vector3(1.0f, 0.0f, _camera.transform.position.z)).x);

		_cubes = new List<GameObject>();

		float x = _levelBounds.x;

		while (x < _levelBounds.y)
		{
			x += _spawnDistance;

			GameObject newCube = CreateCube(0);
			newCube.transform.position = new Vector3(x, transform.position.y);
			newCube.SetActive(true); 
		}
	}

	private void Update()
	{
		MoveCubes(); 
	}

	private GameObject CreateCube(float amplitude)
	{
		GameObject cube = Instantiate(_cubePrefab, transform);
		_cubes.Add(cube); 

		return cube; 
	}

	private void ActivateCube(GameObject cube, float levelHeight)
	{
		if (_invertWave)
			levelHeight = 1.0f - levelHeight;

		float nextAmplitude = _minAmplitude + (_maxAmplitude - _minAmplitude) * (levelHeight);

		_amplitude = Mathf.Lerp(_amplitude, nextAmplitude, 0.5f);

		float sin = _amplitude * Mathf.Sin((Mathf.PI * 2) * Time.time) * _frequency;

		cube.transform.position = new Vector3(_levelBounds.y + _spawnDistance, transform.position.y + sin);
		cube.SetActive(true);
	}

	private void MoveCubes()
	{
		foreach (GameObject cube in _cubes)
		{
			cube.transform.position += new Vector3(-3.0f, 0.0f) * Time.deltaTime;

			if (cube.transform.position.x < _levelBounds.x)
				cube.SetActive(false);
		}
	}

	public void GenerateCube(float levelHeight)
	{
		foreach (GameObject cube in _cubes)
		{
			if (cube.activeSelf)
				continue;

			ActivateCube(cube, levelHeight);
			return;
		}

		GameObject newCube = CreateCube(levelHeight);
		ActivateCube(newCube, levelHeight);
	}


}
