﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuroskyInput : IInputMethod
{
	private TGCConnectionController _connectionController;

	private Vector2 _velocity; 

	public NeuroskyInput()
	{
		_connectionController = GameObject.FindObjectOfType<TGCConnectionController>();

		Debug.Assert(_connectionController != null, "MISSING: TGCConnectionController");

		_connectionController.Parser.UpdateAttentionEvent += OnUpdateAttention; 
	}

	private void OnUpdateAttention(int value)
	{
		_velocity = new Vector2(0.0f, value); 
	}

	public Vector2 GetInput()
	{
		return _velocity; 
	}
}
