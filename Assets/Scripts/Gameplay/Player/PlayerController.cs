﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour
{
	[SerializeField]
	private PlayerSettings _settings;

	private TGCConnectionController _connectionController;

	[SerializeField]
	private float _jumpForce;

	private Rigidbody2D _rigidbody;

	public bool godmode;

	private Vector2 _levelBounds;

	private float _screenHeight;

	[SerializeField]
	private float _minWallOffset;

	private IInputMethod _inputMethod;

	public Action GameOverEvent;

	public Action<int> ScoreEvent;

	private void Awake()
	{
		_connectionController = FindObjectOfType<TGCConnectionController>(); 

		SetInputMethod(_settings.inputMethod);

		Camera cam = Camera.main;

		_levelBounds = new Vector2(
			cam.ViewportToWorldPoint(new Vector3(0.0f, 0.0f, cam.transform.position.z)).y,
			cam.ViewportToWorldPoint(new Vector3(0.0f, 1.0f, cam.transform.position.z)).y);

		_screenHeight = _levelBounds.y - _levelBounds.x - (_minWallOffset * 2);

		if (_settings.inputMethod == InputMethod.Keyboard)
		{
			_rigidbody = GetComponent<Rigidbody2D>();
			_rigidbody.bodyType = RigidbodyType2D.Dynamic;
		}
	}

	private void Update()
	{
		Vector2 input = _inputMethod.GetInput();

		if (_settings.inputMethod != InputMethod.Neurosky)
			return;

		float y = (_levelBounds.x + _minWallOffset) + ((input.y / 100) * _screenHeight);
		transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, y), Time.deltaTime);

	}

	private void LateUpdate()
	{
		if (_settings.inputMethod != InputMethod.Keyboard)
			return;

		if (Input.GetMouseButtonDown(0))
			_rigidbody.velocity = new Vector2(0.0f, _jumpForce);

		if (transform.position.y < _levelBounds.x)
			Debug.Log("Death");

	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		Pickup pickup = collision.gameObject.GetComponent<Pickup>();

		if (!godmode && pickup == null && collision.tag != "SafeZone")
		{
			if (GameOverEvent != null)
				GameOverEvent();

			gameObject.SetActive(false);

			return;
		}

		if (pickup != null)
			pickup.PickUp();

		if (ScoreEvent != null)
			ScoreEvent(1);
	}

	private void SetInputMethod(InputMethod method)
	{
		switch (method)
		{
			case InputMethod.Keyboard: { _inputMethod = new KeyboardInput(); } break;
			case InputMethod.Neurosky: { _inputMethod = new NeuroskyInput(); } break;
			case InputMethod.Emotiv:
				{
					Debug.Log("Emotiv has not yet been implemented");

					_inputMethod = new KeyboardInput();
				}
				break;
		}
	}
}

