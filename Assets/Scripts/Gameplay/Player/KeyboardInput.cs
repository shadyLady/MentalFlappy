﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : IInputMethod
{
	public Vector2 GetInput()
	{
		float v = Input.GetAxis("Vertical");
		return new Vector2(0.0f, v); 
	}
}
