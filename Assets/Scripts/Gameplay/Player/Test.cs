﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour {
	public int explosionForce;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.W))
		{
			Rigidbody2D rb = GetComponent<Rigidbody2D>();
			rb.AddForce(transform.up * explosionForce);
		}
	}
}
