﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputMethod
{
	Keyboard, 
	Neurosky, 
	Emotiv
};

public interface IInputMethod
{
	Vector2 GetInput(); 
}

